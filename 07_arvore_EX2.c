#include <stdio.h>

#include <stdlib.h>

typedef struct arvore {
  int info;
  struct arvore * esq;
  struct arvore * dir;
}
Arvore;

Arvore * cria_arv_vazia(void);
Arvore * arv_constroi(int c, Arvore * e, Arvore * d);
int verifica_arv_vazia(Arvore * a);
Arvore * arv_libera(Arvore * a);
int arv_pertence(Arvore * a, char c);
void arv_imprime(Arvore * a);

Arvore * cria_arv_vazia(void) {
  return NULL;
}

void padding(char ch, int n) {
  int i;
  for (i = 0; i < n; i++)
    putchar(ch);
}

Arvore * arv_constroi(int c, Arvore * e, Arvore * d) {
  Arvore * a = (Arvore * ) malloc(sizeof(Arvore));
  a -> info = c;
  a -> esq = e;
  a -> dir = d;
  return a;
}

int verifica_arv_vazia(Arvore * a) {
  return (a == NULL);
}

Arvore * arv_libera(Arvore * a) {
  if (!verifica_arv_vazia(a)) {
    arv_libera(a -> esq);
    arv_libera(a -> dir);
    free(a);
  }
  return NULL;
}

void structure90(Arvore * root, int level) {
  int i;

  if (root == NULL) {
    padding('\t', level);
    puts("~");
  } else {
    structure90(root -> dir, level + 1);
    padding('\t', level);
    printf("%d\n", root -> info);
    structure90(root -> esq, level + 1);
  }
}

void structureWithLevel(Arvore * root, int level) {
  if (root != NULL) {
    printf("%d\t NLV: %d \t", root -> info, level);
    if (root -> esq == NULL && root -> dir == NULL) {
      printf("No folha\n");
    } else {
      printf("\n");
      structureWithLevel(root -> esq, level + 1);
      structureWithLevel(root -> dir, level + 1);
    }
  }
}

void structureInOrdem(Arvore * root, int level) {
  if (root != NULL) {
    structureInOrdem(root -> esq, level + 1);
    printf("%d\t ", root -> info, level);
    structureInOrdem(root -> dir, level + 1);
  }
}

void structurePreOrdem(Arvore * root, int level) {
  if (root != NULL) {
    printf("%d\t ", root -> info, level);
    structurePreOrdem(root -> esq, level + 1);
    structurePreOrdem(root -> dir, level + 1);
  }
}

void structurePosOrdem(Arvore * root, int level) {
  if (root != NULL) {
    structurePosOrdem(root -> esq, level + 1);

    structurePosOrdem(root -> dir, level + 1);
    printf("%d\t ", root -> info, level);
  }
}

int conta_nos(Arvore * a) {
  if (a == NULL)
    return 0;

  int res = 0;
  res++;
  int auxa = conta_nos(a -> dir);
  int auxb = conta_nos(a -> esq);
  res += (auxa + auxb);
  return res;
}

int max_arvore(Arvore * a) {
  if (a == NULL)
    return 0;

  int res = a -> info;
  int auxa = max_arvore(a -> dir);
  int auxb = max_arvore(a -> esq);
  if (auxa >= res && auxa >= auxb) {
    res = auxa;
  }
  if (auxb >= res && auxb >= auxa) {
    res = auxb;
  }
  return res;
}

int altura_arvores(Arvore * a) {
  if (a == NULL)
    return 0;

  int res = 0;
  res++;
  int auxa = altura_arvores(a -> dir);
  int auxb = altura_arvores(a -> esq);

  if (auxa >= auxb) {
    res += auxa;
  } else {
    res += auxb;
  }
  return res;
};

int nos_folha_arvore(Arvore * a) {
  if (a == NULL)
    return 0;

  int res = 0;
  if (a -> dir && a -> esq) res++;
  res += (nos_folha_arvore(a -> dir) + nos_folha_arvore(a -> esq));

  return res;
};

int main(int argc, char * argv[]) {
  Arvore * a, * a1, * a2, * a3, * a4, * a5, * a6, * a7, * a8;
  a1 = arv_constroi(10, cria_arv_vazia(), cria_arv_vazia());
  a2 = arv_constroi(20, a1, cria_arv_vazia());
  a3 = arv_constroi(40, cria_arv_vazia(), cria_arv_vazia());
  a4 = arv_constroi(30, a2, a3);
  a5 = arv_constroi(98, cria_arv_vazia(), cria_arv_vazia());
  a6 = arv_constroi(95, cria_arv_vazia(), a5);
  a7 = arv_constroi(90, cria_arv_vazia(), a6);
  a = arv_constroi(50, a4, a7);

  printf("PreOrdem: ");
  structurePreOrdem(a, 0);
  
  printf("\nPosOrdem: ");
  structurePosOrdem(a, 0);

  printf("\nInOrdem: ");
  structureInOrdem(a, 0);

  printf("\nNOS: %d", conta_nos(a));
  printf("\nVALOR MAX: %d", max_arvore(a));
  printf("\nALTURA: %d", altura_arvores(a));
  printf("\nNOS COMPLETOS: %d", nos_folha_arvore(a));

  return 0;
}
