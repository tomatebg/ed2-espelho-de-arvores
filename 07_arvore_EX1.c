#include <stdio.h>
#include <stdlib.h>

typedef struct arvore {
  char info;
  struct arvore * esq;
  struct arvore * dir;
}
Arvore;

Arvore * cria_arv_vazia(void);
Arvore * arv_constroi(char c, Arvore * e, Arvore * d);
int verifica_arv_vazia(Arvore * a);
Arvore * arv_libera(Arvore * a);
int arv_pertence(Arvore * a, char c);
void arv_imprime(Arvore * a);

Arvore * cria_arv_vazia(void) {
  return NULL;
}

Arvore * arv_constroi(char c, Arvore * e, Arvore * d) {
  Arvore * a = (Arvore * ) malloc(sizeof(Arvore));
  a -> info = c;
  a -> esq = e;
  a -> dir = d;
  return a;
}

int verifica_arv_vazia(Arvore * a) {
  return (a == NULL);
}

Arvore * arv_libera(Arvore * a) {
  if (!verifica_arv_vazia(a)) {
    arv_libera(a -> esq);
    arv_libera(a -> dir);
    free(a);
  }
  return NULL;
}

void padding(char ch, int n) {
  int i;
  for (i = 0; i < n; i++)
    putchar(ch);
}

void structure90(Arvore * root, int level) {
  int i;

  if (root == NULL) {
    padding('\t', level);
    puts("~");
  } else {
    structure90(root -> dir, level + 1);
    padding('\t', level);
    printf("%c\n", root -> info);
    structure90(root -> esq, level + 1);
  }
}

void structure(Arvore * root, int level) {
  if (root != NULL) {
    printf("%c\t ", root -> info);
    structure(root -> esq, level + 1);
    structure(root -> dir, level + 1);
  }
}

void structureWithAngleBracket(Arvore * root, int level) {
  int i;
  printf(" <");
  if (root == NULL) {
    printf(">");

  } else {

    printf("%c", root -> info);
    structureWithAngleBracket(root -> esq, level + 1);

    structureWithAngleBracket(root -> dir, level + 1);
    printf(" >");
  }
}

void structureWithLevel(Arvore * root, int level) {
  if (root != NULL) {
    printf("%c\t NLV: %d \t", root -> info, level);
    if (root -> esq == NULL && root -> dir == NULL){
	printf("No folha\n");

	}else {
	printf("\n" );
    structureWithLevel(root -> esq, level + 1);
    structureWithLevel(root -> dir, level + 1);
}
  }
}

int main(int argc, char * argv[]) {
  Arvore * a, * a1, * a2, * a3, * a4, * a5;
  a1 = arv_constroi('d', cria_arv_vazia(), cria_arv_vazia());
  a2 = arv_constroi('b', cria_arv_vazia(), a1);
  a3 = arv_constroi('e', cria_arv_vazia(), cria_arv_vazia());
  a4 = arv_constroi('f', cria_arv_vazia(), cria_arv_vazia());
  a5 = arv_constroi('c', a3, a4);
  a = arv_constroi('a', a2, a5);

structureWithLevel(a, 0);

  return 0;
}
