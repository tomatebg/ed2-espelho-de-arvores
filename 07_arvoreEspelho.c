#include <stdio.h>

#include <stdlib.h>

typedef struct arvore {
  char info;
  struct arvore * esq;
  struct arvore * dir;
}
Arvore;

Arvore * cria_arv_vazia(void);
Arvore * arv_constroi(char c, Arvore * e, Arvore * d);
int verifica_arv_vazia(Arvore * a);
Arvore * arv_libera(Arvore * a);
int arv_pertence(Arvore * a, char c);
void arv_imprime(Arvore * a);

Arvore * cria_arv_vazia(void) {
  return NULL;
}


Arvore * arv_constroi(char c, Arvore * e, Arvore * d) {
  Arvore * a = (Arvore * ) malloc(sizeof(Arvore));
  a -> info = c;
  a -> esq = e;
  a -> dir = d;
  return a;
}

int verifica_arv_vazia(Arvore * a) {
  return (a == NULL);
}

Arvore * arv_libera(Arvore * a) {
  if (!verifica_arv_vazia(a)) {
    arv_libera(a -> esq);
    arv_libera(a -> dir);
    free(a);
  }
  return NULL;
}

Arvore * cria_espelho(Arvore * a) {
  if (a == NULL)
    return NULL;

  Arvore * temp = arv_constroi(a -> info, cria_arv_vazia(), cria_arv_vazia());
  temp -> dir = cria_espelho(a -> esq);
  temp -> esq = cria_espelho(a -> dir);
  return temp;
}

int eh_espelho(Arvore * a, Arvore * b) {
  if (a == NULL && b == NULL)
    return 1;

  if (a == NULL || b == NULL)
    return 0;

  return a -> info == b -> info &&
    eh_espelho(a -> esq, b -> dir) &&
    eh_espelho(a -> dir, b -> esq);
}

int main(int argc, char * argv[]) {
  Arvore * a, * a1, * a2, * a3, * a4, * a5, * mirror, * a9;
  a1 = arv_constroi('d', cria_arv_vazia(), cria_arv_vazia());
  a2 = arv_constroi('b', a1, cria_arv_vazia());
  a3 = arv_constroi('e', cria_arv_vazia(), cria_arv_vazia());
  a4 = arv_constroi('f', cria_arv_vazia(), cria_arv_vazia());
  a5 = arv_constroi('c', a3, a4);
  a = arv_constroi('a', a2, a5);
  mirror = a;

  a9 = cria_espelho(mirror);

  printf("%d\n", eh_espelho(a, a9));

  return 0;
}
